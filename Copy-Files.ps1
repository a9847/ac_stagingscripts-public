﻿$TargetDir="C:\Turbine\Asheron's Call\macros" # Directory to copy the files to, should be the fully qualified path to a folder within the AC install directory

# Create the directory if it doesn't exist
if (!(Test-path -Path $TargetDir)){
	New-item -Path $TargetDir -ItemType Directory
}
# Update Files
Get-Childitem -Path "$PSScriptRoot\*" -Exclude *.ps1,.gitignore,.gitattributes|
    Copy-Item -Destination "$TargetDir\" -Force -Verbose
