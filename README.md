#  Public AC Staging Scripts

These scripts are used for tasks such as loading a character profile, meta and then setting a couple of parameters.
I frequently use them to load something like a Character profile for a specific set of Killtasks and then set a few VTank parameters incase they may have drifted from what I usually want them to be set to (such as Attack, Follow distance, etc)

These files should be copied to a folder within your Asheron's Call Installation directory (usually C:\Turbine\Asheron's Call) and then launched via the chat box using the /loadfile command.
For example, if you placed the files in a folder named "macros" you run enter the following in the chat box.
/loadfile macros\Quest_Society.txt



## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.